package org.intelligen;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class IntelliGen
 */
public class IntelliGen extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IntelliGen() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String url1 = ((HttpServletRequest)request).getRequestURL().toString();
		//String queryString = ((HttpServletRequest)request).getQueryString();
		

		response.getWriter().append("Served at: ").append(request.getContextPath()); 
		Connection conn = null;
		Statement stmt = null;
		String dbURL = "";
		
		
		try{  
			
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			if(url1.contains("6060")){
				dbURL = "jdbc:sqlserver://localhost:1433;databaseName=Intelligen_Dev;";
				System.out.println("In 6060");
			}
			else if(url1.contains("3030")){
				dbURL = "jdbc:sqlserver://localhost:1433;databaseName=Intelligen_QA;";
				System.out.println("In 3030");
			}
			else {
				dbURL = "jdbc:sqlserver://35.154.85.200:1433;databaseName=Intelligen_Prod;";
				System.out.println("In 8080");
			}
			conn =  (Connection) DriverManager.getConnection(dbURL,"intelligenuser", "password" );
			if (conn != null) {
			    System.out.println("Connected");
			}
			
			stmt= conn.createStatement();
			String name= request.getParameter("name");
			String email= request.getParameter("email");
			String message = request.getParameter("message");
			String phone_number="";
			if(name!=null && !name.equals("")&& email!=null && !email.equals("")&& message!=null && !message.equals("")){
			if(request.getParameter("phone_number")!=null && !request.getParameter("phone_number").equals("")){
				phone_number = request.getParameter("phone_number");
			}
			
			String query="";
			//String query= "insert into user_info( name , email, message) values(  '"+name+"' , '"+email+"' , '"+message+"')";
			if(phone_number!=null && !phone_number.equals("")){
				query= "insert into user_info( name , email, message, phone_number) values(  '"+name+"' , '"+email+"' , '"+message+"', '"+phone_number+"' )";
			}
			else {
				query= "insert into user_info( name , email, message) values(  '"+name+"' , '"+email+"' , '"+message+"')";
			}
			int i= stmt.executeUpdate(query);
			if(i>1){
				out.println("done plz check");
			}
			System.err.println("no");
			} 
			RequestDispatcher rd = request.getRequestDispatcher("DataServlet");
			rd.forward(request, response);
			
			
			}
			catch(SQLException e){ System.out.println(e);} 
			catch(Exception e1){ System.out.println(e1);}
			
			finally {
			try{
				stmt.close();
				conn.close();
				}
				catch(SQLException e2){ System.out.println(e2);} 
			}
		
	}
		/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}