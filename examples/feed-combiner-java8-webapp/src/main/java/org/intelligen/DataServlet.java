package org.intelligen;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class DataServlet
 */
public class DataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DataServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Data ").append(request.getContextPath());
		
		PrintWriter out = response.getWriter();
		String url1 = ((HttpServletRequest)request).getRequestURL().toString();
		//String queryString = ((HttpServletRequest)request).getQueryString();
		
	

		
		response.setContentType("text/html");
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		//System.out.println("myField----"+request.getParameter("myField"));
		
		try{  
			String dbURL = "";
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			if(url1.contains("6060")){
				dbURL = "jdbc:sqlserver://localhost:1433;databaseName=Intelligen_Dev;";
				System.out.println("In 6060");
			}
			else if(url1.contains("3030")){
				dbURL = "jdbc:sqlserver://localhost:1433;databaseName=Intelligen_QA;";
				System.out.println("In 3030");
			}
			else {
				dbURL = "jdbc:sqlserver://35.154.85.200:1433;databaseName=Intelligen_Prod;";
				System.out.println("In 8080");
			}
				
			conn =  (Connection) DriverManager.getConnection(dbURL,"intelligenuser", "password" );
			if (conn != null) {
			    System.out.println("Connected");
			}
			//Connection conn1 =  (Connection) DriverManager.getConnection(dbURL,"intelligenuser", "password" );
			//if (conn1 != null) {
			 //   System.out.println("Connected 1");
			//}
			out.print("<head>" +
					" <title>Intelligen Data</title>" +
  "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>" +
  "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>" +
  "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>" +
					"</head>");
			
				
			stmt= conn.createStatement();
			//Statement stmt1= conn1.createStatement();
			String query = "Select TOP 5 * from user_info ORDER BY id desc";
			//String query1="SELECT top 1 * FROM user_info";
			rs = stmt.executeQuery(query);
			//ResultSet rs1 = stmt1.executeQuery(query1);
			ResultSetMetaData rsmd = rs.getMetaData();

			int columnsNumber = rsmd.getColumnCount();
			System.out.println("columnsNumber"+columnsNumber);
					
			out.print("<div class='container'>");
			out.print("<table class='table table-striped'>");
			out.print("<thead>");
			out.print("<tr>");
			out.print("<th>  Id  </th> ");
			out.print("<th>  Name  </th> ");
			out.print("<th>  Email  </th> ");
			if(columnsNumber == 5){
			out.print("<th>  Phone Number </th> ");
			}
			out.print("<th>  Message </th> ");
			out.print("</tr>");
			out.print("</thead>");
			out.print("<tbody>");
			while (rs.next()){
				out.print("<tr>");
				out.print("<td> "+rs.getString("id")+"  </td> ");
				out.print("<td> "+rs.getString("name")+"  </td> ");
				out.print("<td> "+rs.getString("email")+"   </td> ");
				if(columnsNumber == 5){
				out.print("<td> "+rs.getString("phone_number")+"  </td> ");
				}
				out.print("<td> "+rs.getString("message")+"  </td> ");
			
				out.print("</tr>");
			}
			out.print("</tbody>");
			out.print("</table>");
			out.print("</div>");
			
		}catch(SQLException e){ System.out.println(e);} 
		catch(Exception e1){ System.out.println(e1);}
		
		finally {
		try{
			rs.close();
			stmt.close();
			conn.close();
			}
			catch(SQLException e2){ System.out.println(e2);} 
		}
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}